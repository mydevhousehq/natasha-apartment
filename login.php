<?php
include('functions.php');

session_start();


$table = "tbl_user";
$user = $_POST['user'];
$pass = $_POST['pass'];


$get_pass = get_pass($user, $table);
$get_id = get_id($user);
$get_access = get_access($get_id);

if ($get_pass ==""){
    $_SESSION['error_msg'] = "Invalid Username and Password. Please try again.";
    header("Location: index.php");
}

elseif (isset($get_pass)) {
	$result = hashpass($pass, $get_pass);
	if ($result==true) {
        $_SESSION['ID'] = $get_id;
        $access = get_access($get_id);
        if ($access=="Admin"){
            header("Location: dashboard.php");
        
        }else{
            header("Location: dashboard.php");
        }
}else {
    $_SESSION['error_msg'] = "Password is incorrect. Please try again.";
    header("Location: index.php");
}

}

?>