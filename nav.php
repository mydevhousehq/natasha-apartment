<div class="logo">
        <a href="dashboard.php" class="simple-text logo-mini">
          Apartment Finder
        </a>
        
</div>
<div class="sidebar-wrapper">
        <ul class="nav">
          <li class="nav-item ">
            <a class="nav-link" href="dashboard.php">
              <i class="material-icons">dashboard</i>
              <p>Dashboard</p>
            </a>
          </li>

          <?php 
          $access= $login_access;
          
          if ($access == "Admin"){

        
         
             echo "<li class='nav-item '>
             <a class='nav-link' href='user.php'>
               <i class='material-icons'>person</i>
               <p>Users</p>
             </a>
           </li>
           <li class='nav-item '>
            <a class='nav-link' href='listing.php'>
              <i class='material-icons'>business</i>
              <p>Listing</p>
            </a>
          </li>
          <li class='nav-item '>
            <a class='nav-link' href='categories.php'>
              <i class='material-icons'>category</i>
              <p>Categories</p>
            </a>
          </li>";
          
          }
          
          ?>
         
    
          
          <li class="nav-item ">
            <a class="nav-link" href="#">
              <i class="material-icons">location_ons</i>
              <p>Maps</p>
            </a>
          </li>

          
          <li class="nav-item active-pro ">
            <a class="nav-link" href="logout.php">
              <i class="material-icons">logout</i>
              <p>Logout</p>
            </a>
          </li>
        </ul>
      </div>
    </div>
    <div class="main-panel">
     
      
