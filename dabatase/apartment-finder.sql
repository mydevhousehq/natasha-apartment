-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 15, 2020 at 01:09 AM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `apartment-finder`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_categ`
--

CREATE TABLE `tbl_categ` (
  `id` int(11) NOT NULL,
  `category` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_categ`
--

INSERT INTO `tbl_categ` (`id`, `category`) VALUES
(1, 'test'),
(3, 'test1');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_listing`
--

CREATE TABLE `tbl_listing` (
  `ID` int(11) NOT NULL,
  `title` text NOT NULL,
  `longitude` text NOT NULL,
  `latitude` text NOT NULL,
  `category` text NOT NULL,
  `terms` text NOT NULL,
  `owner` text NOT NULL,
  `contact` text NOT NULL,
  `rate` text NOT NULL,
  `address` text NOT NULL,
  `status` text NOT NULL,
  `details` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_listing`
--

INSERT INTO `tbl_listing` (`ID`, `title`, `longitude`, `latitude`, `category`, `terms`, `owner`, `contact`, `rate`, `address`, `status`, `details`) VALUES
(28, 'Studio Type Shit', '17.675984', '121.725485', 'test', '200', 'Xavier Francis Mendoza', '09975863422', '25', 'Cagayan, Carig Norte', 'Active', '<p>asdasdad</p>'),
(29, 'Studio Type Shit', '17.675984', '121.725485', 'test', '200', 'Xavier Francis Mendoza', '09975863422', '25', 'Cagayan, Carig Norte', 'Active', '<p>asdasdad</p>'),
(30, 'Studio Type Shit', '17.675984', '121.725485', 'test', '200', 'Xavier Francis Mendoza', '09975863422', '25', 'Cagayan, Carig Norte', 'Active', '<p>asdsfdghgnmj,jaf</p>');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pictures`
--

CREATE TABLE `tbl_pictures` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `file_name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_pictures`
--

INSERT INTO `tbl_pictures` (`id`, `title`, `file_name`) VALUES
(18, 'Studio Type Shit', 'api/uploads/photo-1494030575520-dd03dd6aeb04.1579046906.jpg'),
(19, 'Studio Type Shit', 'api/uploads/photo-1494030575520-dd03dd6aeb04.1579046916.jpg'),
(20, 'Studio Type Shit', 'api/uploads/photo-1494030575520-dd03dd6aeb04.1579046947.jpg'),
(21, 'Studio Type Shit', 'api/uploads/photo-1537462715879-360eeb61a0ad.1579046947.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `ID` int(11) NOT NULL,
  `username` text NOT NULL,
  `password` text NOT NULL,
  `access` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`ID`, `username`, `password`, `access`) VALUES
(1, 'admin', '$2y$10$7zDAlZSZdQkDWHKe0FDdze5/vKKhYwNwvuG5xv4ZubtrDSYPWllDC', 'Admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_categ`
--
ALTER TABLE `tbl_categ`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_listing`
--
ALTER TABLE `tbl_listing`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_pictures`
--
ALTER TABLE `tbl_pictures`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_categ`
--
ALTER TABLE `tbl_categ`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_listing`
--
ALTER TABLE `tbl_listing`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `tbl_pictures`
--
ALTER TABLE `tbl_pictures`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
