<?php include('header.php');

$table_name="tbl_categ";


if(isset($_GET['delete'])){
    $id  = $_GET['delete'];
    delete($id,$table_name);
}

?>
<body>

  <div class="wrapper ">
    <div class="sidebar" data-color="purple" data-background-color="white">

    <?php include('nav.php');?>




    <div class="content">
        <div class="container-fluid ">
    
        <div class="card mt-0">
        <div class="card-header">
            <h3> Add Categories </h3>


        </div>
        <div class="card-body">
        <form action="add_categ.php" method="POST">

        <fieldset>
	    <?php
			if (isset($_SESSION['success'])) {
		?>
		<div class="alert alert-success" style="margin: 24px;">
		<?= $_SESSION['success'] ?>
		</div>
		<?php
		}
		unset($_SESSION['success']);
		?>
    </fieldset>

        <fieldset>
	    <?php
			if (isset($_SESSION['error_msg'])) {
		?>
		<div class="alert alert-warning" style="margin: 24px;">
		<?= $_SESSION['error_msg'] ?>
		</div>
		<?php
		}
		unset($_SESSION['error_msg']);
		?>
    </fieldset>
    

  <div class="form-group col-6">
    <input type="text" class="form-control" name="categ" placeholder="Category" required/>
  </div>
  <br>
  
  
  
 
 
  <button type="submit" class="btn btn-primary">Submit</button>

     

        </form>
        </div>
        </div>


        <div class="card mt-0">
        <div class="card-header">
           <h3>  Category List</h3>


        </div>
        <div class="card-body">
        <table class="table table-hover">
                <thead class="thead-light">
                    <tr>
                     
                        <th>Category</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                   <?php
                      
                        $user_data = get($table_name);

                        foreach($user_data as $key => $row){
                            $categ = $row['category'];
                            $id = $row['id'];
                
                        


                   ?>
                   <form method="POST" action="">
                   <tr>
                        <input type="hidden" name="id" value="<?= $id ?>">
                        <td><?= $categ ?></td>
                  
                       
                        <td>
                            <a href="test.php?id=<?= $id ?>" class="btn btn-warning btn-sm">
                                <i class="material-icons">create</i>
                            </a>


                            </td>

					</tr>
					<?php } ?>

                        </form>
                </tbody>
                </table>
        </div>
        </div>

        </div>
      </div>


<?php include('footer.php');?>
    </div>
  </div>
</body>

</html>