<?php include('header.php');

$table_name="tbl_listing";


if($_SERVER['REQUEST_METHOD']=="POST"):
  


    $con=Connection();
    
    

    
    

    $title=$_POST['title'];
    $category=$_POST['category'];
    $address=$_POST['address'];
    $owner=$_POST['owner'];
    $contact=$_POST['contact'];
    $terms=$_POST['terms'];
    $rate=$_POST['rate'];
    $status=$_POST['status'];
    $longitude = $_POST['longitude'];
    $latitude = $_POST['latitude'];
    $Details = $_POST['Details'];
    
    



                  $q1 = mysqli_query($con, "INSERT INTO `tbl_listing`( `title`, `longitude`, `latitude`, `category`, `terms`, `owner`, `contact`, `rate`,`address`,`status`,`details`) VALUES ('$title','$longitude','$latitude','$category','$terms','$owner','$contact','$rate','$address','$status','$Details')");
  
                    if($q1){
                        $_SESSION['success']="Apartment Successfully Added.";

                        extract($_POST);
                        $error=array();
                        $extension=array("jpeg","jpg","png","gif");
                        foreach($_FILES["files"]["tmp_name"] as $key=>$tmp_name) {
                            $file_name=$_FILES["files"]["name"][$key];
                            $file_tmp=$_FILES["files"]["tmp_name"][$key];
                            $ext=pathinfo($file_name,PATHINFO_EXTENSION);

                            if(in_array($ext,$extension)) {
                                if(!file_exists("api/uploads/".$file_name)) {
                                    move_uploaded_file($file_tmp=$_FILES["files"]["tmp_name"][$key],"api/uploads/".$file_name);
                                }
                                else {
                                    $filename=basename($file_name,$ext);
                                    $newFileName=$filename.time().".".$ext;
                                    move_uploaded_file($file_tmp=$_FILES["files"]["tmp_name"][$key], $file_des="api/uploads/".$newFileName);
                            
                                    $q2 = mysqli_query($con, "INSERT INTO `tbl_pictures`( `title`, `file_name`) VALUES ('$title','$file_des')");
                              
                                }
                            }
                            else {
                                array_push($error,"$file_name, ");
                            }
                        }
                        
                    }else{
                        $_SESSION['error_msg']="There was a problem adding Apartment!";
                    }
 
                 
    
    

    

 
    
    
    
    endif



?>
<body>
  <div class="wrapper ">
    <div class="sidebar" data-color="purple" data-background-color="white">

    <?php include('nav.php');?>




    <div class="content">
        <div class="container-fluid ">
    
        <div class="card mt-0">
        <div class="card-header">
            <h3> Add Apartment </h3>


        </div>
        <div class="card-body">
        <form action="<?php echo $_SERVER['PHP_SELF'];?>" method="post" enctype="multipart/form-data">

        <fieldset>
	    <?php
			if (isset($_SESSION['success'])) {
		?>
		<div class="alert alert-success" style="margin: 24px;">
		<?= $_SESSION['success'] ?>
		</div>
		<?php
		}
		unset($_SESSION['success']);
		?>
    </fieldset>

        <fieldset>
	    <?php
			if (isset($_SESSION['error_msg'])) {
		?>
		<div class="alert alert-warning" style="margin: 24px;">
		<?= $_SESSION['error_msg'] ?>
		</div>
		<?php
		}
		unset($_SESSION['error_msg']);
		?>
    </fieldset>
    

  <div class="form-group">
    <input type="text" class="form-control" name="title" placeholder="Title" required/>
  </div>
  <br>
  <div class="row">
    <div class="col">
      <input type="text" name="address" class="form-control" placeholder="Address" required/>
    </div>
  </div>
  <br>
  <div class="row">
    <div class="col">
      <input type="text" name="owner" class="form-control" placeholder="Owner" required/>
    </div>

  </div>
  <br>

  <br>
  <div class="row">
    <div class="col">
      <input type="number" name="rate" class="form-control" placeholder="Rate" required/>
    </div>
    <div class="col">
      <input type="number" name="contact" class="form-control" placeholder="Contact Details" required/>
    </div>
  </div>
  <br>
  <div class="row">
    <div class="col">
      <input type="text" name="longitude" class="form-control" placeholder="Longitude" required/>
    </div>
    <div class="col">
      <input type="text" name="latitude" class="form-control" placeholder="Latitude" required/>
    </div>
  </div>
  <br>
  <div class="row">
  <div class="col">
  <div class="form-group">
    <label for="exampleFormControlSelect1">Category</label>
    <select name="category" class="form-control">
     <?php 
     $table = "tbl_categ";
         $user_data1 = get($table);

         foreach($user_data1 as $key => $row){
             $categ = $row['category'];
        
     ?>
      <option value="<?= $categ ?>"><?= $categ ?></option>
         <?php } ?>
    </select>
  </div>
  </div>
  <div class="col">
  <div class="form-group">
    <label for="exampleFormControlSelect1">Status</label>
    <select name="status" class="form-control">
      <option value="Active">Active</option>
      <option value="Active">Inactive</option>
    </select>
  </div>
  </div>
  </div>
  <br>

  <div class="form-group">
  <label for="exampleFormControlSelect1">Terms & Agreement</label>
  <textarea name="terms" class="textarea"></textarea>
  </div><Br>

  <div class="form-group">
  <label for="exampleFormControlSelect1">Details</label>
  <textarea name="Details" class="textarea"></textarea>
  </div><Br>
          <label>Upload Image:</label><br>
      <input class="btn btn-primary btn-sm" type="file" id="file" name="files[]" required multiple/>


  <br><br>
  <input type="submit" name="submit" value="submit" id="but_upload" class="btn btn-primary">

     

        </form>
        </div>
        </div>


        <div class="card mt-0">
        <div class="card-header">
           <h3>  Apartment List</h3>


        </div>
        <div class="card-body">
        <table class="table table-hover">
                <thead class="thead-light">
                    <tr>
                     
                        <th>Title</th>
                        <th>Category</th>
                        <th>Address</th>
                        <th>Owner</th>
                        <th>Contact</th>
                        <th>Terms</th>
                        <th>Rate</th>
                        <th>Status</th>
                        <th>Longitude</th>
                        <th>Latitude</th>
                        <th></th>
                    
                  
                   
                    </tr>
                </thead>
                <tbody>
                   <?php
                      
                        $user_data = get($table_name);

                        foreach($user_data as $key => $row){
                            $title = strtoupper($row['title']);
                            $category = strtoupper($row['category']);
                            $address = strtoupper($row['address']);
                            $owner = $row['owner'];
                            $contact = $row['contact'];
                            $terms = $row['terms'];
                            $rate = $row['rate'];
                            $status = $row['status'];
                            $longitude = $row['longitude'];
                            $latitude = $row['latitude'];
                        


                   ?>
                   <form method="POST" action="">
                   <tr>
                        <input type="hidden" name="id" value="<?= $id ?>">
                        <td><?= $title ?></td>
                        <td><?= $category ?></td>
                        <td><?= $address ?></td>
                        <td><?= $owner ?></td>
                        <td><?= $contact ?></td>
                        <td ><?= $terms ?></td>
                        <td ><?= $rate ?></td>
                        <td ><?= $status ?></td>
                        <td ><?= $longitude ?></td>
                        <td ><?= $latitude ?></td>
                        <td>
                            <a href="test.php?id=<?= $id ?>" class="btn btn-warning btn-sm">
                                <i class="material-icons">create</i>
                            </a>
                        
						
						    <a class="btn btn-danger btn-sm" id="delete_btn" href="?delete=<?= $id ?>">
								<i class="material-icons">delete</i> 
                            </a>
                            </td>

					</tr>
					<?php } ?>

                        </form>
                </tbody>
                </table>
        </div>
        </div>

        </div>
      </div>


<?php include('footer.php');?>
    </div>
  </div>


  <script>
tinymce.init({
    selector: ".textarea",
    plugins: [
        "advlist autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table paste"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
});



  $(document).ready(function(){

$("#but_upload").click(function(){

    var fd = new FormData();
    var files = $('#file')[0].files[0];
    fd.append('file',files);

    $.ajax({
        url: 'upload.php',
        type: 'post',
        data: fd,
        contentType: false,
        processData: false,
        success: function(response){
            if(response != 0){
                $("#img").attr("src",response); 
                $(".preview img").show(); // Display image element
            }else{
                alert('file not uploaded');
            }
        },
    });
});
});
  
  </script>
</body>

</html>