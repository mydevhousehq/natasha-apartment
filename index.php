<?php
include('functions.php');
session_start();


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/index.css">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <title>Login</title>
</head>
<body>

<div class="wrapper fadeInDown">
  <div id="formContent">
    <!-- Tabs Titles -->
    <br>
    <!-- Icon -->
    <div class="fadeIn first">

      <img src="assets/img/user.png" id="icon" alt="User Icon" /><br>
      <h3>Login Form</h3>
    </div>
    <br>
    <!-- Login Form -->
    <form action="login.php" method="POST">
    <fieldset>
	    <?php
			if (isset($_SESSION['error_msg'])) {
		?>
		<div class="alert alert-warning" style="margin: 24px;">
		<?= $_SESSION['error_msg'] ?>
		</div>
		<?php
		}
		unset($_SESSION['error_msg']);
		?>
	</fieldset>

      <input type="text" id="login" class="fadeIn second" name="user" placeholder="Username">
      <input type="password" id="password" class="fadeIn third" name="pass" placeholder="Password">
      
      <input type="submit" class="fadeIn fourth" value="Log In">
    </form>

    <!-- Remind Passowrd -->
    <div id="formFooter">
      <a class="underlineHover" href="#">Forgot Password?</a>
    </div>

  </div>
</div>
</body>
</html>