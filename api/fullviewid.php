<?php 
include('conn.php');
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");


if(isset($_GET['id'])){

    $sid=$_GET['id'];
    $st = "SELECT * FROM `tbl_listing` WHERE ID=$sid";
   
    $json_response = array(); //Create an array

    $cm = $conn->prepare($st);
    $cm->execute();
   
    while($row = $cm->fetch(PDO::FETCH_ASSOC)){
        $row_array = array();
        $row_array['ID']= $row['ID'];
        $row_array['title'] = $row['title'];  
        $row_array['longitude'] = $row['longitude'];  
        $row_array['latitude'] = $row['latitude'];  
        $row_array['category'] = $row['category'];  
        $row_array['terms'] = $row['terms'];  
        $row_array['owner'] = $row['owner'];  
        $row_array['contact'] = $row['contact']; 
        $row_array['rate'] = $row['rate']; 
        $row_array['address'] = $row['address']; 
        $row_array['status'] = $row['status']; 
        $row_array['details'] = $row['details']; 


        $sid = $row['title'];  
        $st2 = "select * from tbl_pictures where title='$sid'";
        $cm2 = $conn->prepare($st2);
         $cm2->execute();

         while($nested = $cm2->fetch(PDO::FETCH_ASSOC)){
            $row_array['picture'][] = array(
                'id' => $nested['id'],
                'file_name' => $nested['file_name'],
            );
    
        }
        array_push($json_response, $row_array); //push the values in the array
    }


    echo json_encode($json_response);
}else{
    die();
}
