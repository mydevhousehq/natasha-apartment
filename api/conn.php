<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

try {
    $constring = 'mysql:host=localhost;dbname=apartment-finder'; // host and db
    $user= 'root'; // define the username
    $pass=''; // password
    
    $conn = new PDO($constring, $user, $pass);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $conn;
    } catch (PDOException $e) {
        $msg = $e -> getmessage();
        echo "ERROR LIST ->".$msg;
        die();
    }

    