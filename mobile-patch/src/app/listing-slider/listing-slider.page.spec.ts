import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ListingSliderPage } from './listing-slider.page';

describe('ListingSliderPage', () => {
  let component: ListingSliderPage;
  let fixture: ComponentFixture<ListingSliderPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListingSliderPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ListingSliderPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
