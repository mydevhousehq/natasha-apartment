import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListingSliderPageRoutingModule } from './listing-slider-routing.module';

import { ListingSliderPage } from './listing-slider.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListingSliderPageRoutingModule
  ],
  declarations: [ListingSliderPage]
})
export class ListingSliderPageModule {}
