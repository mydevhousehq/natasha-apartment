import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';
import {environment} from '../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class ServiceService {
  constructor(private http: HttpClient) { }
  public api = environment.api;
  public httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    })
  };
  private extractData(res: Response) {
    let body = res;
    return body || { };
  }

//get data
getdata(): Observable<any> {
  const apiurl = this.api+'fulldata';
  console.log('apiurl::::::::'+apiurl);
  return this.http.get(apiurl).pipe(
    map(this.extractData));
}

//data + id
getdataid(id): Observable<any> {
  const apiurl = this.api + 'fullviewid?id='+id ;
  console.log('apiurl::::::::'+apiurl);
  return this.http.get(apiurl).pipe(
    map(this.extractData));
}

//search title
searchtitle(title): Observable<any> {
  const apiurl = this.api + 'searchtitle?id='+title ;
  console.log('apiurl::::::::'+apiurl);
  return this.http.get(apiurl).pipe(
    map(this.extractData));
}



}
