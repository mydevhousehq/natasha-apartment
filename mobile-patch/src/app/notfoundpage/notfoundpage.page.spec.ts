import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { NotfoundpagePage } from './notfoundpage.page';

describe('NotfoundpagePage', () => {
  let component: NotfoundpagePage;
  let fixture: ComponentFixture<NotfoundpagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotfoundpagePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(NotfoundpagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
