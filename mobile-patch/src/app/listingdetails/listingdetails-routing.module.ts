import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListingdetailsPage } from './listingdetails.page';


const routes: Routes = [
  {
    path: '',
    component: ListingdetailsPage
  },
  {
    path: ':id',
    component: ListingdetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListingdetailsPageRoutingModule {}
