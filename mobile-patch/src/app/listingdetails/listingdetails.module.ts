import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListingdetailsPageRoutingModule } from './listingdetails-routing.module';

import { ListingdetailsPage } from './listingdetails.page';
import {ComponentsModule} from '../components/components-import.module';
import {environment} from '../../environments/environment';
import { AgmCoreModule } from '@agm/core';
@NgModule({
  imports: [ComponentsModule,
    CommonModule,
    FormsModule,
    IonicModule,
    ListingdetailsPageRoutingModule,
    AgmCoreModule.forRoot({
      apiKey: environment.gmap_api
    })
  ],
  declarations: [ListingdetailsPage]
})
export class ListingdetailsPageModule {}
