import { Component, OnInit } from '@angular/core';
import {ServiceService} from '../service.service';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { Platform } from '@ionic/angular';
@Component({
  selector: 'app-listingdetails',
  templateUrl: './listingdetails.page.html',
  styleUrls: ['./listingdetails.page.scss'],
})
export class ListingdetailsPage implements OnInit {
  id;
  public datax : Array<any> = [];
  picture = {};
 
 



  constructor(public rest:ServiceService, private route: ActivatedRoute, private router: Router , public platform: Platform) {


  
   }
  
   private dataq: any[]=[];
  public  lat: number = 0;
  public  lng: number = 0;
   

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
    this.rest.getdataid(this.id).subscribe(res => {
      this.datax = res;
     
      this.lat = this.datax[0].latitde;
      this.lng = this.datax[0].longitude;
      
    });
  }



  api_picture(api){

    api = environment.picture_api + api;
    return api;
  }
  

}
