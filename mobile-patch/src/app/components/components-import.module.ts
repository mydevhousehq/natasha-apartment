import {NgModule} from '@angular/core';
import {TestComponent} from './test/test.component'; //example1
import {Test2Component} from './test2/test2.component'; //example2
import { CommonModule } from '@angular/common';
import {HeaderComponent} from './header/header.component';
import {  CUSTOM_ELEMENTS_SCHEMA , NO_ERRORS_SCHEMA } from '@angular/core';
import {SearchbarComponent} from './searchbar/searchbar.component';
import {SlidesComponent} from './slides/slides.component';
import { Routes, RouterModule } from '@angular/router';
@NgModule({
declarations:[TestComponent,Test2Component,HeaderComponent,SearchbarComponent,SlidesComponent],
exports:[TestComponent,Test2Component,HeaderComponent,SearchbarComponent,SlidesComponent],
imports:[CommonModule,RouterModule ],
schemas:[ CUSTOM_ELEMENTS_SCHEMA , NO_ERRORS_SCHEMA ]
})
export class ComponentsModule{}