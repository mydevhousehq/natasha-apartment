import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ListingdataComponent } from './listingdata.component';

describe('ListingdataComponent', () => {
  let component: ListingdataComponent;
  let fixture: ComponentFixture<ListingdataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListingdataComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ListingdataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
