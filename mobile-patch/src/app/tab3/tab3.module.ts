import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Tab3Page } from './tab3.page';
import {ComponentsModule} from '../components/components-import.module';
import { AgmCoreModule } from '@agm/core';
import {environment} from '../../environments/environment';
@NgModule({
  imports: [ComponentsModule,
    IonicModule,
    CommonModule,
    FormsModule,
    RouterModule.forChild([{ path: '', component: Tab3Page }]),
    AgmCoreModule.forRoot({
      apiKey: environment.gmap_api
    })
  ],
  declarations: [Tab3Page]
})
export class Tab3PageModule {}
