import { Component } from '@angular/core';
import { Platform } from '@ionic/angular';
@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {
  title: string = 'Mapa';

  
  height = 0;
  constructor(public platform: Platform) {
    console.log(platform.height());
    this.height = platform.height() - 56;
  }

}
